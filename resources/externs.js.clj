(ns extern-js
  (:require [clojure.java.io :as io]
            [clojure.string :as str])
  (:import java.nio.file.attribute.FileAttribute
           java.nio.file.Files
           java.nio.file.Paths))

(println "Downloading the externs file of Chrome Extensions...")

(let [output-file (subs *file* 0 (- (count *file*) (count ".clj")))
      [browser] *command-line-args*
      url (cond
            (not= browser "safari")
            "https://raw.githubusercontent.com/google/closure-compiler/master/contrib/externs/chrome_extensions.js"
            (=    browser "safari")
            "https://raw.githubusercontent.com/google/closure-compiler/master/contrib/externs/safari.js")
      externs (slurp url)]
  (spit output-file (cond-> externs
                      (not= browser "safari")
                      (str/replace #"(?m)^chrome\." "browser."))))
