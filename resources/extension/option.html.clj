(ns option-html
  (:require [clojure.java.io :as io]
            [hiccup.core :refer [html]]))

(println "Generating option.html...")

(let [output-file (subs *file* 0 (- (count *file*) 4))
      proj (->> "project.clj"
                (slurp)
                (read-string)
                (drop 3)
                (apply hash-map))
      [browser build-id] *command-line-args*
      build-id (or (keyword build-id) :dev)
      compile-option (get-in proj [:cljsbuild :builds build-id :compiler])
      js-out-file-name (-> compile-option :output-to (io/as-file) (.getName))]
  (->> [:html
        [:head
         [:title "feedly-anime-feed-helper options"]]
        [:body
         [:div
          "Google API Key[" [:a {:href "https://developers.google.com/custom-search/json-api/v1/overview?csw=1" :target "_blank"} "1"] "]:"
          [:input#api {:type "text"}]]
         [:div
          "Google CSE Key[" [:a {:href "https://cse.google.com/all" :target "_blank"} "2"] "]:"
          [:input#cse {:type "text"}]]
         [:script {:src "polyfill.js"}]
         [:script {:src js-out-file-name}]
         [:script {:src "option.js"}]]]
       (html)
       (spit output-file)))
