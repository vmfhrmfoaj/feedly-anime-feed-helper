(ns manifest-json
  (:refer-clojure :exclude [read-string])
  (:require[clojure.data.json :as json]
           [clojure.edn :refer [read-string]]
           [clojure.java.io :as io]
           [clojure.string :as str]))

(println "Generating manifest.json...")

(let [output-file (subs *file* 0 (- (count *file*) 4))
      proj (read-string (slurp "project.clj"))
      [_ name version] proj
      proj (->> proj
                (drop 3)
                (apply hash-map))
      [browser build-id] *command-line-args*
      build-id (or (keyword build-id) :dev)
      js-out-file-name (-> proj
                           (get-in [:cljsbuild :builds build-id :compiler :output-to])
                           (io/as-file)
                           (.getName))
      manifest (cond-> {:manifest-version 2
                        :name (str name)
                        :description (:description proj)
                        :version (first (str/split version #"-"))
                        :permissions ["tabs" "storage" "https://feedly.com/*" "https://www.googleapis.com/customsearch/v1*"]
                        :browser-action {:default-icon "icon.png"}
                        :background {:scripts  [js-out-file-name "background.js"] :persistent false}
                        :content-scripts [{:js [js-out-file-name "content-script.js"]
                                           :run-at "document_idle"
                                           :matches ["https://feedly.com/*"]}]}
                 :default
                 (-> (update-in [:background :scripts]   #(vec (cons "polyfill.js" %)))
                     (update-in [:content-scripts 0 :js] #(vec (cons "polyfill.js" %))))
                 (= "chrome"  browser)
                 (assoc :options-page "option.html")
                 (= "firefox" browser)
                 (assoc :options-ui {:page "option.html"})
                 (= :dev build-id)
                 (assoc :content-security-policy
                        "script-src 'self' 'unsafe-eval'; object-src 'self'"))]
  (spit output-file
        (json/write-str manifest
                        :escape-slash false
                        :key-fn #(str/replace (clojure.core/name %) #"-" "_"))))
