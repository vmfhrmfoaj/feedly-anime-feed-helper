(defproject feedly-anime-feed-helper
  "0.1.2-SNAPSHOT"

  :description "Provides more rich information for anime feeds."
  :url "https://gitlab.com/vmfhrmfoaj/feedly-anime-feed-helper"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :main dev.repl

  :clean-targets ^{:protect false} [:target-path
                                    "resources/externs.js"
                                    "resources/extension/main.js"
                                    "resources/extension/manifest.json"
                                    "resources/extension/option.html"]

  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/clojurescript "1.10.339"]
                 [org.clojure/core.async "0.4.474"]]

  :plugins [[cider/cider-nrepl "0.18.0-SNAPSHOT"]
            [lein-cljsbuild "1.1.7"]]

  :profiles
  {:dev {:source-paths ["tool"]
         :dependencies [[hiccup "1.0.5"]
                        [org.clojure/tools.nrepl "0.2.13" :exclusions [org.clojure/clojure]]
                        [com.cemerick/piggieback "0.2.2"]
                        [figwheel-sidecar "0.5.15"]]
         :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
         :figwheel {:reload-clj-files {:clj false :cljc true}}}}

  :cljsbuild
  {:builds {:dev {:source-paths ["src" "tool"]
                  :compiler {:output-to "resources/extension/main.js"
                             :output-dir "target/cljsbuild-dev"
                             :externs ["resources/externs.js"]
                             :closure-output-charset "US-ASCII"
                             :optimizations :whitespace
                             :pretty-print true
                             :parallel-build true}
                  :figwheel {:before-jsload dev.tool/on-compile
                             ;; NOTE
                             ;;  If target site is on HTTPS, you cannot use Figwheel directly.
                             ;;  You need a proxy to convert HTTPS to HTTP.
                             ;;  See, https://github.com/bhauman/lein-figwheel/issues/365
                             ;; :websocket-url "wss://localhost:34490/figwheel-ws"
                             :websocket-host "localhost"}}
            :prod {:source-paths ["src"]
                   :compiler {:output-to "resources/extension/main.js"
                              :output-dir "target/cljsbuild-prod"
                              :externs ["resources/externs.js"]
                              :closure-output-charset "US-ASCII"
                              :optimizations :advanced
                              :pretty-print false
                              :parallel-build true}}}}

  :aliases
  {"chrome"
   ["do"
    ["clean"]
    ["run" "-m" "clojure.main" "resources/externs.js.clj" "chrome"]
    ["cljsbuild" "once" "prod"]
    ["trampoline" "run" "-m" "clojure.main" "resources/extension/manifest.json.clj" "chrome" "prod"]
    ["trampoline" "run" "-m" "clojure.main" "resources/extension/option.html.clj"   "chrome" "prod"]]

   "chrome-dev"
   ["do"
    ["cljsbuild" "once" "dev"]
    ["trampoline" "run" "-m" "clojure.main" "resources/extension/option.html.clj"   "chrome"]
    ["trampoline" "run" "-m" "clojure.main" "resources/extension/manifest.json.clj" "chrome"]]

   "firefox"
   ["do"
    ["clean"]
    ["run" "-m" "clojure.main" "resources/externs.js.clj" "firefox"]
    ["cljsbuild" "once" "prod"]
    ["trampoline" "run" "-m" "clojure.main" "resources/extension/manifest.json.clj" "firefox" "prod"]
    ["trampoline" "run" "-m" "clojure.main" "resources/extension/option.html.clj"   "firefox" "prod"]]

   "firefox-dev"
   ["do"
    ["cljsbuild" "once" "dev"]
    ["trampoline" "run" "-m" "clojure.main" "resources/extension/option.html.clj"   "firefox"]
    ["trampoline" "run" "-m" "clojure.main" "resources/extension/manifest.json.clj" "firefox"]]})
