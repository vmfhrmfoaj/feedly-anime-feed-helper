(ns feedly-anime-feed-helper.content-script.core
  (:require [cljs.core.async :as async])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(def title-selector "[id$='entry_title']")

(defn title []
  (.-innerText (.querySelector js/document title-selector)))

(defn anime? []
  (let [title (title)]
    (println "title:" (str "\"" title "\""))
    (and (re-find #"^\[.+-Raws\]" title) title)))

(defn anime-name
  [title]
  (let [[_ name] (re-find #"\]\s(.+)\s-" title)]
    (println "anime name:" name)
    name))


(def api-key
  (atom ""))

(def cse-key
  (atom ""))

(defn str=>json
  [s]
  (.parse js/JSON s))

(def anime-info-cache (atom nil))

(defn search-ch
  [name]
  (let [query (str "https://www.googleapis.com/customsearch/v1" "?"
                   "key=" @api-key "&"
                   "cx=" @cse-key "&"
                   "q=" name "&"
                   "searchType=image")
        out (async/chan)]
    (if-let [res (get @anime-info-cache name)]
      (async/put! out res)
      (let [xhttp (js/XMLHttpRequest.)]
        (println "Send a query:" query)
        (set! (.-onreadystatechange xhttp)
              (fn []
                (this-as this
                  (when (= 4 (.-readyState this))
                    (println "Recive a response" (str "(status:" (.-status this) ")."))
                    (if (= 200 (.-status this))
                      (let [res (-> (.-responseText this)
                                    (str=>json)
                                    (js->clj :keywordize-keys true)
                                    :items)]
                        (swap! anime-info-cache assoc name res)
                        (async/put! out res))
                      (do
                        (println "Request error:" (.-statusText this))
                        (async/close! out)))))))
        (.open xhttp "GET" query true)
        (.send xhttp)))
    out))

(defn anime-info-ch
  [name]
  (let [in (search-ch (js/encodeURIComponent name))]
    (go
      (let [[{:keys [link image]}]
            (filter #(< (get-in % [:image :width])
                        (get-in % [:image :height]))
                    (async/<! in))]
        {:title name
         :img {:url link
               :thumb-url (:thumbnailLink image)
               :w (:width image)
               :h (:height image)}}))))


(def content-selector "[id$='entryContent']")

(defn insert-image
  [elem {:keys [url thumb-url w h]}]
  (let [bg-size "background-size: 100% 100%;"
        bg-img (str "background-image:url(" thumb-url ");")
        width  (str "width:"  w "px;")
        height (str "height:" h "px;")
        html (str "<div style=\"" bg-size " " bg-img " " width " " height "\">"
                  "  <img src='" url "'/>"
                  "</div>")]
    (.insertAdjacentHTML elem "afterbegin" html)))

(defn insert-info
  [ch]
  (go
    (when-let [elem (.querySelector js/document content-selector)]
      (let [{:keys [img] :as info} (<! ch)]
        (println "Insert a anime info:" info)
        (insert-image elem img)))))


(def last-url (atom nil))

(defn url-changed?
  [url]
  (if-not (= url @last-url)
    (do
      (println "URL changed:" url)
      (reset! last-url url)
      true)))

(defn feedly-entry-url?
  [url]
  (boolean (re-find #"/entry/" url)))

(defn ^:export index []
  (.. js/browser -runtime
      (sendMessage ""
                   (fn [option]
                     (println "option:" option)
                     (reset! api-key (aget option "api-key"))
                     (reset! cse-key (aget option "cse-key")))))
  (.setInterval js/window
                (fn []
                  (let [url (.. js/window -location -href)]
                    (if (and (url-changed? url)
                             (feedly-entry-url? url))
                      (some-> (anime?)
                              (anime-name)
                              (anime-info-ch)
                              (insert-info)))))
                150))

;; NOTE
;; During development, print the log.
(enable-console-print!)
