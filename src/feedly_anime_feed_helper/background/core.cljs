(ns feedly-anime-feed-helper.background.core)

(defonce ^:private option
  (let [opt (atom nil)
        update! (fn [& _]
                  (.. js/browser
                      -storage
                      -local
                      (get #(reset! opt (js->clj % :keywordize-keys true)))))]
    (update!)
    (.. js/browser
        -storage
        -onChanged
        (addListener update!))
    opt))

(defn event-handler
  [_ _ cb]
  (println "Event occurred, option:" @option)
  (cb (clj->js @option)))

(defn ^:export index []
  (.. js/browser
      -runtime
      -onMessage
      (addListener event-handler)))

;; NOTE
;; During development, print the log.
(enable-console-print!)
