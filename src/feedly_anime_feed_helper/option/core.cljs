(ns feedly-anime-feed-helper.option.core)

(defn save []
  (.. js/browser
      -storage
      -local
      (set #js{:api-key (.. js/document (getElementById "api") -value)
               :cse-key (.. js/document (getElementById "cse") -value)})))

(defn restore []
  (.. js/browser
      -storage
      -local
      (get #js{:api-key ""
               :cse-key ""}
           (fn [items]
             (set! (.. js/document (getElementById "api") -value) (aget items "api-key"))
             (set! (.. js/document (getElementById "cse") -value) (aget items "cse-key"))))))


(defn ^:export index []
  (.. js/document (getElementById "api") (addEventListener "change" save))
  (.. js/document (getElementById "cse") (addEventListener "change" save))
  (.addEventListener js/document "DOMContentLoaded" restore))
