(ns dev.tool)

;; NOTE
;; To avoid inserting the script tag.
(set! (.. js/goog -global -CLOSURE_IMPORT_SCRIPT) (constantly true))

(defn on-compile []
  (if-let [runtime (some-> js/browser (.-runtime))]
    (.reload runtime)
    (.reload js/location)))
